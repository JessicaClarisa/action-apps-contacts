package com.development.transfermoney_dummyapplication

internal data class Data(val name: String, val amount: String)