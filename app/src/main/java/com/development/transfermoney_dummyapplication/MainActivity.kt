package com.development.transfermoney_dummyapplication

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.devrel.android.fitactions.DeepLink

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_awal)

        val button = findViewById<Button>(R.id.btnNext)
        button.setOnClickListener{
            val intent = Intent(this, TransferActivity::class.java)
            intent.putExtra("isShortcut","false")
            startActivity(intent)
        }

        // Handle the intent this activity was launched with.
        intent?.handleIntent()

    }

    /**
     * Handle new intents that are coming while the activity is on foreground since we set the
     * launchMode to be singleTask, avoiding multiple instances of this activity to be created.
     *
     * See [launchMode](https://developer.android.com/guide/topics/manifest/activity-element#lmode)
     */
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.handleIntent()
    }

    /**
     * Handles the action from the intent base on the type.
     *
     * @receiver the intent to handle
     */
    private fun Intent.handleIntent() {
        when (action) {
            // When the action is triggered by a deep-link, Intent.Action_VIEW will be used
            Intent.ACTION_VIEW -> handleDeepLink(data)
            // Otherwise start the app as you would normally do.
            else -> showDefaultView()
        }
    }

    /**
     * Use the URI provided by the intent to handle the different deep-links
     */
    private fun handleDeepLink(data: Uri?) {
        // path is normally used to indicate which view should be displayed
        // https://transfermoney.page.link/transfer?text=, name} --> path = "transfer"

        when (data?.path) {
            DeepLink.TRANSFER -> {
                val getText = data.getQueryParameter(DeepLink.Params.TEXT).toString()
                val getName = data.getQueryParameter(DeepLink.Params.NAME).toString()

                //pop up + masukkin datanya
                updateView(getName, getText)
            }

            else -> {
                // path is not supported or invalid, start normal flow.
                showDefaultView()
            }
        }

    }

    private fun showDefaultView() {
        MainActivity::class.java
    }

    private fun updateView(mName: String, mText: String) {
        val intent = Intent(this, TransferActivity::class.java)
        intent.putExtra("name",mName)
        intent.putExtra("text",mText)
        intent.putExtra("isShortcut","true")
        startActivity(intent)
    }

}